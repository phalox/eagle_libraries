# Phalox Eagle libraries

This repo contains Eagle libraries that I often use. They might come in handy for others, but more importantly; I won't loose them any longer!



###Part of these libraries are copyright of others. Please refer to their websites for more information about their license###
+ dp_devices ~ [Dangerous Prototypes](http://dangerousprototypes.com/)
+ Itead_BP_110825.lbr ~ [Itead Studio](http://www.iteadstudio.com/)
+ ITEADstudio_DRC.dru Design rule checks ~ [Itead Studio)
+ SparkFun-Connectors ~ [SparkFun](http://www.sparkfun.com/)
+ Seeed_Gerber_Generater_v0r95_DrillAlign ~ [Seeed Studio](http://www.seeedstudio.com/)
+ Fusion_eagle_rule_v1.1 ~ [Seeed Studio]


###My personal library is licensed under the Creative Commons Attribution license v3.0

This means that you are free:

+ **to Share** � to copy, distribute and transmit the work
+ **to Remix** � to adapt the work
+ to make commercial use of the work

Under the following conditions:
**Attribution** � You must attribute the work in the manner specified by the author or licensor

**Attribution** in this case means that you should please provide a link to [Phalox.be](http://phalox.be/ "Phalox.be")

More information on this license can be found [here](http://creativecommons.org/licenses/by/3.0/ "CC BY")

![CC BY](http://i.creativecommons.org/l/by/3.0/88x31.png)